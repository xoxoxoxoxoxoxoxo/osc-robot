package me.feng.chatterbot;
import me.feng.osc.OscActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class Main {

	public static void main(String[] args) {
		ActorSystem system = ActorSystem.create("system");
		ActorRef actor = system.actorOf(Props.create(OscActor.class),
				"listener");
		actor.tell("login", actor);
		while (true) {
			actor.tell("check", actor);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
