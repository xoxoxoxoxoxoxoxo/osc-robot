package me.feng.osc

import scala.beans.BeanProperty
import org.jsoup.Jsoup
import scala.collection.immutable.List
import scala.collection.mutable.ArrayBuffer

class Active {

  @BeanProperty
  var id: String = _

  @BeanProperty
  var catalog: String = _

  @BeanProperty
  var replyid: String = _

  @BeanProperty
  var authorid: String = _

  @BeanProperty
  var message: String = _

  @BeanProperty
  var replymsg: String = _
}

object Active {

  def parseList(xml: String): Array[Active] = {
    val list = ArrayBuffer[Active]()
    val ele = Jsoup.parse(xml)
    val actives = ele.select("active")
    if (!actives.isEmpty()) {
      for (i <- 0 until actives.size()) {
        val activeEle = actives.get(i)
        val active = new Active
        active.setId(activeEle.select("id").first().html())
        active.setAuthorid(activeEle.select("authorid").first().html())
        active.setCatalog(activeEle.select("catalog").first().html())
        var msg = activeEle.select("message").first().text()
        if(msg.startsWith("@小香蕉")){
          msg = msg.substring("@小香蕉".length())
        }
        active.setMessage(msg)
        active.setReplyid(activeEle.select("objectID").first().html())
        list += active
      }
    }
    list.toArray
  }
}