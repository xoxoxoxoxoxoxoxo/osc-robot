package Main

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler
import org.apache.commons.httpclient.HttpClient
import org.apache.commons.httpclient.cookie.CookiePolicy
import org.apache.commons.httpclient.params.HttpMethodParams
import org.apache.commons.httpclient.methods.GetMethod
import java.net.URLEncoder
import org.apache.commons.httpclient.URI

object Main {

  def main(args: Array[String]) {
    val client = new HttpClient

    // 设置 HttpClient 接收 Cookie,用与浏览器一样的策略
    client.getParams().setCookiePolicy(
      CookiePolicy.BROWSER_COMPATIBILITY);
    // 设置 默认的超时重试处理策略
    client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
      new DefaultHttpMethodRetryHandler());
    // 设置 连接超时时间
    client.getHttpConnectionManager().getParams()
      .setConnectionTimeout(20000);
    // 设置 读数据超时时间
    client.getHttpConnectionManager().getParams()
      .setSoTimeout(20000);
    // 设置 字符集
    client.getParams().setContentCharset("GB2312");

    var content = "中文"
   // content = new String(content.getBytes("UTF-8"), "GB2312");
    var url = "http://3g.wuxi.gov.cn/contentsearch/getJson?jsonpCallback=?&query=" +URLEncoder.encode(content) +"&sitename=site_1374209144805&countperpage=5&pagenum=1"
  //  val url = "http://3g.wuxi.gov.cn/contentsearch/getJson?jsonpCallback=?&query=%25E6%2594%25BF%25E5%258A%25A1&sitename=site_1375781314296&countperpage=10&pagenum=1&_=1376035286739"
    println(url)
    val method = new GetMethod(url)
    method.setRequestHeader("Host", "3g.wuxi.gov.cn")
    method.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    method.setRequestHeader("Accept-Language", "zh-CN,zh;q=0.8")
    method.setRequestHeader("Connection", "keep-alive")
    method.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.72 Safari/537.36")

    client.executeMethod(method)
    
    val result = method.getResponseBodyAsString()
    
    println(result)
    
    val s = "展板"
    println(URLEncoder.encode(s))
    println(URLEncoder.encode(URLEncoder.encode(s)))
  }
}